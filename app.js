const express = require('express')
const app = express()
const port = 3000

const si = require('systeminformation');
const moremath = require('moremath');
const store = require('data-store')({ path: process.cwd() + '/config.json' });
var five = require("johnny-five");
var exec = require('child_process').exec;
let virtualTemp=0;
let virtualRes =-1;
let full_speed_temperature = 55;
let maxCpuTemperature = 60;
let maxGpuTemperature = 80;
let maxVirtualTemperature=100;
let loopSeconds=15;
let minRes =82;
let min = 50;
let COM = "COM3";
let nvdia_smi = "nvidia-smi";
var board;

let pin9; let pin11;

function load(){
    if(store.has("nvdia-smi")){
        nvdia_smi = store.get("nvdia-smi");
    }
    else{
        store.set("nvdia-smi",nvdia_smi);
        store.save();
    }
    if(store.has("min-res")){
        minRes = Number(store.get("min-res"));
    }
    else{
        store.set("min-res",minRes);
        store.save();
    }

    if(store.has("full-speed-temperature")){
        full_speed_temperature = Number(store.get("full-speed-temperature"));
    }
    else{
        store.set("full-speed-temperature",full_speed_temperature);
        store.save();
    }
    if(store.has("loop-seconds")){
        loopSeconds = Number(store.get("loop-seconds"));
    }
    else{
        store.set("loop-seconds",loopSeconds);
        store.save();
    }
    if(store.has("max-virtual-temperature")){
        maxVirtualTemperature = Number(store.get("max-virtual-temperature"));
    }
    else{
        store.set("max-virtual-temperature",maxVirtualTemperature);
        store.save();
    }

    if(store.has("max-CPU-temperature")){
        maxCpuTemperature = Number(store.get("max-CPU-temperature"));
    }
    else{
        store.set("max-CPU-temperature",maxCpuTemperature);
        store.save();
    }

    if(store.has("max-GPU-temperature")){
        maxGpuTemperature = Number(store.get("max-GPU-temperature"));
    }
    else{
        store.set("max-GPU-temperature",maxGpuTemperature);
        store.save();
    }

    if(store.has("min")){
        min = Number(store.get("min"));
    }
    else{
        store.set("min",min);
        store.save();
    }
    
    if(store.has("COM")){
        COM = store.get("COM");
    }
    else{
        store.set("COM","COM4");
        store.save();
    }
    try{
        board = new five.Board({
            port: COM
          });
        board.on("ready", function() {
            this.pinMode(11, five.Pin.PWM);
            this.pinMode(9, five.Pin.PWM);
            pin9 = this.analogWrite(9, 0);
            pin11 = this.analogWrite(11, 0);
            readTemp();
        });
    }catch(error){
        console.error(error);
        process.exit(1);
    }
    

    

}
//nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader
function readTemp(){
    si.cpuTemperature(function(cpu){
        exec(nvdia_smi + ' --query-gpu=temperature.gpu --format=csv,noheader', function (error, gpu) {
            var tempCPU = Math.round(Number(cpu.max));
            var tempGPU = Math.round(Number(gpu));
            var res=0;
            var fullSpeedOn =false;

            var resVirtualTemp = Math.round(moremath.map(moremath.clamp(virtualTemp, min, maxVirtualTemperature), min, maxVirtualTemperature,0,255));
            var resCPU = Math.round(moremath.map(moremath.clamp(tempCPU, min, maxCpuTemperature), min, maxCpuTemperature,0,255));
            var resGPU = Math.round(moremath.map(moremath.clamp(tempGPU, min, maxGpuTemperature), min, maxGpuTemperature,0,255));            
            if(virtualTemp== 0){
                if(resCPU>=resGPU){
                    res = resCPU;
                    if(tempCPU>=full_speed_temperature ){
                        fullSpeedOn=true;
                    }
                    else{
                        fullSpeedOn=false;
                    }
                    
                }else{
                    res = resGPU;
                    if(tempGPU>=full_speed_temperature ){
                        fullSpeedOn=true;
                    }
                    else{
                        fullSpeedOn=false;
                    }
                }
            }
            else{
                res = resVirtualTemp;
                if(virtualTemp>=full_speed_temperature ){
                    fullSpeedOn=true;
                }
                else{
                    fullSpeedOn=false;
                }
            }
            if(virtualRes>-1){
                res = virtualRes;
            }
            else{
                if(res > 0 && res<minRes){
                    res = minRes;
                }
                
            }

            
            

            pin11.analogWrite(11,res);
            if(fullSpeedOn ==true){
                pin9.analogWrite(9,255);
            }
            else{
                pin9.analogWrite(9,0);
            }
            
            console.log("CPU="+ tempCPU+" GPU="+tempGPU+ " | "+res);
            
            setTimeout(readTemp,loopSeconds*1000);
        });
        
    });
}

load();

app.get('/setTemp/:temp', (req, res) =>{
    try{
        virtualTemp = Math.round(Number(req.params.temp));
    }
    catch(err){
        virtualTemp = 0;
    }
    
    res.send("Setted temperanture = "+virtualTemp);
})

app.get('/setRes/:res', (req, res) =>{
    try{
        virtualRes = Math.round(Number(req.params.res));
    }
    catch(err){
        virtualRes = 0;
    }
    
    res.send("Setted res = "+virtualRes);
})

app.listen(port, () => console.log(`App listening on port ${port}!`))

